import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { AngularFireModule } from "angularfire2";

// New imports to update based on AngularFire2 version 4
import { AngularFireDatabaseModule } from "angularfire2/database";
import { AngularFireAuthModule } from "angularfire2/auth";

export const firebaseConfig = {
  apiKey: "AIzaSyBOpM1f4azhdaQuK8ZwJi52IH6A0dak8EI",
  authDomain: "se-task.firebaseapp.com",
  databaseURL: "https://se-task.firebaseio.com",
  projectId: "se-task",
  storageBucket: "se-task.appspot.com",
  messagingSenderId: "9858377894"
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
