# SE-Task

* This is my submission for the Software Engineering Junior TA position. I used Angular v5 and Firebase in order to complete the task. Nodejs was not required to complete the task but since it was in the description, I added a small express server that serves the built frontend created by angular. The actual angular code is included in a folder named angular as well. This code is there for you to do whatever you like with. However, running the node server actually serves the minified, compressed version of this code found in the public directory.

## Usage

Running the server:

```
npm install && npm start
```

The server will start on port 3000

## Demo

A deployed demo can be found [here](https://se-task.herokuapp.com/) 

## About Me
- Name : Ahmed El Sayegh
- ID : 31-12573
- Frontend developer with 2 year experience
